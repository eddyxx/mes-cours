## Angular creer un module de page

### A. C'est quoi un module ?

Angular est décompossé en module , c'est a dire que l'on va découper l'application en plusieurs modules, et chaque module va nous rendre un but

#### 1.Les types de module:

**type page** = contient l'integralité d'une page

* Si il y a une page qui s'appel home = creer un module qui va s'appeler home

**type component** = module dans le quel on va pouvoir mettre des components qu'on va pouvoir re-utiliser partout

 dans ce module , on va y mettre tout le htlm scss qui va être la structure de la page

Ensuite , la page va contenir  plusieurs components re-utilisable et comme on peut les re-utiliser , on va pouvoir les mettre dans un module de component ré-utilisable.

=>  Ex ; 

* Si on a un header et un footer , on va pouvoir  importer ces modules dans les modules principaux.

---------------------------------------------------------------------------------------------------------------------------------------------->

On va creer un nouveau module de type page (qui a une route) et on va accrocher cet page a [app-routing ].

**Dans le terminal Webstorm**

````
=> ng g m home --route=home --module=app-routing
````

[app-routing] => ce sont les routes de la racine de l'application.

[ng (g)enerate] => je fabrique quelque chose

[ m home] => on va fabriquer une page "home"

[home] => nom du module crée

[--module = app-routing] => element avec lequel va être rattacher le module que l'on a creer


````
ctrl + shift + A  => Ajouter un élément dans gitlab
````
--------------------------------------------------------------------------------------------------
#### 2.Les decorateurs

Les decorateurs sont des meta-données

le decorateur sert a dire que l'on a une class de type homemodule , cette class est decorée avec un decorateur NgModule => qui va expliquer a angular comment va fonctionner la class homeModule.

Elle va fonctionner en important d'autres modules ;

````
Ex:

@NgModule({

  declarations: [HomeComponent],

  imports: [

    CommonModule,

    HomeRoutingModule

  ]

})

export class HomeModule { }


* [declaration] => c'est le homecomponant , c'est a dire qu'a l'interieur du module , il y a un component contrairement a react.

* [commonModule] => module ds lequel il y a des info de base Angular (module obligatoire)

* [homeComponent] => il s'agit d'une class qui s'appel HomeComponent et qui implements un init

* homeRoutingModule =>
````
#### 3. Comment creer des liens ?**

```` 
 Ex:
 
 <ul>
 
 <li>
 
 <a routerLink="home">home</a>
 
 </li>
 
 <a routerLink="a propos">a propos</a>

 </li>
 
 </ul>

 <router-outlet></router-outlet>
 
 ````
 
 ///video115 + video116///
