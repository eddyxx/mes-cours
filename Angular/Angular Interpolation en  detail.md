## Angular Interpolation en detail

### A. Rêgle de l'interpolation avec Angular

Interpolation => calcule d'un élément, recuperation du resultat et  l'afficher.

Quand on a des objets spéciaux de type ;
 
 * une date 
 
 * un objet 
 
 * une classe particuliere ....
 
  On utilise [Pipe].

Avec une interpolation en angular , on n'a seulement le droit d'aller chercher une valeur et de l'afficher.


**Dans le fichier menu1.component.htlm**

On peut avoir une operation 
````
Bonjour, {{1 + 1}}
````
* Angular va calculer les élèments se trouvant dans l'interpolation

**Methode in class**

on va creer une fonction dans le fichier menu1.component.ts

sayHello(){

return 'hello'

}
Ensuite dans le fichier menu1.component.htlm , on va ajouter :

````
Bonjour, {{sayHello}}
````
('hello' va apparaitre sur la page localhost de l'application)

* On ne peut pas utiliser if() en interpolaion !!!

* Avec l'interpolation en angular , il n'y a que 2 maniéres d'afficher un objet.
------------------------------------------------------------------------------------------>



### B.Comment afficher un objet en interpolation ?

Methode 1 => Pipe [|] :

**Dans le fichier menu1.component.ts**

Fait passer dans un tuyau(pipe) ,l'éléments contenu dans sayhello() et le transforme en json.

Ex:

````
Bonjour, {{sayHello()  | json }}
````

**Dans le fichier menu1.component.htlm**

````
sayHello(){

return {age: 32, name:'jean-michel'};
````

Methode 2:

````
Bonjour, {{sayHello().age}} {{sayHello().name}}
````
 ///video119///
