## Angular Les Inputs

On n a des routes commune a tte l'appli mais l'objet (l'info principal) est a l'interieur de allroutes , personne n'a accés a l'info a part le component lui même

#### A. Comment passer des variables d'un component a un autre ?

**Dans menu1.component.ts**

=> Prendre l'info contenue dans allRoutes (couper/coller)

**Dans app.component.ts**

=> On va dans app menu1 (commande + app-menu1)

=> on va creer un input

````
export class MenuComponent inplements OnInit {

@Input() routes: MenuModel[];

name: string;

age = 42

Comme Input() est vide Routes est un array vide et donc il n'y a pas d'erreurs.
````
[Inputs] Cest une props speciale qui doit être declarer trés clairement (explicitement).

On va creer un @input() ,  un decorateur qui doit être importer par principe.

**Dans app.component.htlm**

````
<H1 class="main">mon app</H1>

<app-menu1 [routes]= "allRoutes"></app-menu1>

<router-outlet></router-outlet>
````
Les routes s'affiche bien dans l'appli en ligne.

#### B. comment faire en sorte que l'age ne sois plus d'un component mais qui nous vient du pére ?

**Dans menu1.component.ts**


On duplique @Input puis remplacer routes par age et donc de type number

export class MenuComponent inplements OnInit {

@Input() routes: MenuModel[];

@Input() age: number;

name: string;

**Dans app.component.ts**

On va creer une variable age dans laquel on va mettre une valeur num

````
name: string;

allRoutes: MenuModel [] = [

{url: 'home', name: 'accueil'},

{url: 'à-propos', name: 'à propos'}

];

age = 54;
````

**Dans app.component.htlm**

On va passer la variable de cet façon :

````
<H1 class="main">mon app</H1>

<app-menu1 [routes]= "allRoutes" [age]= [age]></app-menu1>

Si on veut afficher l'age:

<div>j'ai {{ age}} ans</div>

<ng-container *ngFor= "let current of allRoutes">

<li *ngFor="let current of allRoutes">

<a
 
 [routerLink]= "current.url">
 
 {{current.name}}
 
 </a>

</li>

</ng-container>

</ul>
````
Déléguation des compétences =

[menu1] sais ce qu'il doit afficher grace au NG If et NG For

[le component principal(app.component.ts)] doit declarer des variables et les passer aux gens qui en ont besoin.
