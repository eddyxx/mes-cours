## Angular NG FOR

Dans l'interpolation , on n'a pas le droit de faire des map(), ni des boucles ou quoi que ce soit.

### A. Comment faire  des boucles for en Angular ?

En Angular , quand on veut faire des boucles for , on utilise une directive [*ngFor]

 une directive => petit outil, un attribut Angular qui permet de donner des "super-pouvoir" au dom

et qui va nous permettre de réitérer un élément (faire une boucle)


**Dans menu.component.ts**

Ex:

````
On va remplacer [link1 = 'home']; par allRoutes

export class Menu1Component implements OnInit {

name: string;

allRoutes: string [] = ['home','à-propos'];

constructor () {

}
````

**Dans menu1.component.htlm**

on va faire une boucle for ou on va parcourir toutes les routes

On va creer une variable let qui va s'appeler current of allRoutes

````
<app-menu-logo></app-menu-logo>

<ul>

Bonjour, {{sayHello().age}} {{sayHello().name}}

<li *ngFor=" "let current of allRoutes">

<a [routelink]= "current">{{current}}</a>

</li>

</ul>
````
Les interpolation a l'interieur d'une balise, peuvent être faites de cet façon => ["current">{{current}}</a>]

--------------------------------------------------------------------------------------------------------

**Comment avoir un objet ou "à propos" et" home " seront les urls et qui afficheront un nom different?

**Dans menu1.component.ts**

````
name: string;

allRoutes: any [] = [

{url: 'home', name: 'accueil'},

{url: 'à-propos', name: 'à propos'}];

````
Pour corriger on corrige , il nous faut un type ;

=> Dans  app

=> Creation de nouveau projet  = Menu.Model.ts

**Dans Menu.Modele.ts**

On export interface MenuModel 
````
Ex:

export interface MenuModel {

 url : string;
 
name : string;

}
````

 **Dans menu1.component.ts**
 
 On remplace any qui a été mis par defaut , par MenuModel
 
````
name: string;

allRoutes: MenuModel [] = [

{url: 'home', name: 'accueil'},

{url: 'à-propos', name: 'à propos'}];

````

**Dans menu1.component.htlm**

Current est un objet et on a pas le droit de mettre de routerLink  dans un objet.

**Probleme:**
````
<app-menu-logo></app-menu-logo>

<ul>

Bonjour, {{sayHello().age}} {{sayHello().name}}

<li *ngFor=" "let current of allRoutes">

<a [routelink]= "current">{{current}}</a>

</li>

</ul>
````
**Solution**
````
<app-menu-logo></app-menu-logo>

<ul>

Bonjour, {{sayHello().age}} {{sayHello().name}}

<li *ngFor=" "let current of allRoutes">

<a [routelink]= "current.url">{{current.name}}</a>

</li>

</ul>
````
On obtient une belle boucle for ainsi qu'un menu interactif qui m'affiche une url différente du message  qui est afficher et dans laquelle je peut naviguer.

///video121///
