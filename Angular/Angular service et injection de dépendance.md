## Angular service et injection de dépendance

Les components sont la pour gerer les actions de l'utilisateur

quand l'utilisateur clic sur quelque chose on va handle le clic

En angular les données vont dans un service.

#### A.Comment on créer un service ?

On va creer un dossier dans lequel il y'aura tout les services commun a l'application 

**Dans Terminal**

=> ng g s services/menu

creation du dossier services 

**Dans menu.service.ts**

les services est un injectable c'est un decorateur qui donne des super pouvoir  =

On va pouvoir stocker des infos dans les services et les injecter la ou on en a besoin

**Dans app.component.ts**

couper/coller une data (allRoutes) puis la coller dans le fichier menu.service.ts

[au dessus du constructor] car c'est un attribut.

Rappel ordre du code:

 * attribut  
 
 * constructor 
  
 * méthodes

#### B.Comment injecter un service ? 

On creer le construtor() , 

dans le constructor on va lui dire que l'on va lui injecter un service et que ce service va être utiliser que dans app.component

(personne ne pourra bidouiller le service)

````
allRoutes: MenuModel[] = this.menuService.allRoutes;

age = 32;

constructor(

private menuService :MenuService

) {

}
````

* Moyen d'accéder directement a une info sans passer par les props ,on a mis en place les services

Pour que menu component puisse accéder au service:

**Dans menu1.component.ts**

export class Menu1Component implements OnInit {

@Input() age: number;

name: string;

constructor (

private menuService :MenuService,

) {

}

 [l'injection de dépendance ]= on va injecter un service a l'interieur d'un component, les 2 components on accés a la même info en tps réel.

**raccourcie**

shift + fn + f6 = pour selectionner menuService


///video124///
