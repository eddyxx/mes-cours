## Angular créer un module de components

### A . Comment faire un module a part dont on peut ce servir  ailleurs dans l'application ?

**Dans le terminal Webstorm**

creation du module ('main-menu.module.ts')

````
=> ng g m components/main-menu  
````
* Module dans lequel on va mettre plusieurs components 

Comme nous avons 2 main-menu (home, à-propos) on va créer 2 sous components

````
=> ng g c components/main-menu/menu1

=> ng g c components/main-menu/menu2
````

* Angular Creer tous ce qui est necessaire pour le menu 1 et menu 2

* Angular va mettre a jour le fichier main-menu => ajout de fichier sup

 Creer un component logo qui ne va apparaitre que dans le menu 1 et le menu 2

sans que quelqu'un puisse s'en servir a l'exterieur...

**Dans Terminal Webstorm**

````
ng g c components/main-menu/menu-logo
````
**Dans menu2.component.htlm**

* Quand on fabrique quelque chose avec Angular , on doit mettre un prefix 

le prefix par default est app.

```` 
=> <app-menu-logo><menu-logo/>
````
[ app-menu-logo]  est un component a part entiére, peut être utiliser ds Menu1 et Menu2 mais personne d'autre.

les gens qui sont en dehors du module ne pourron pas avoir accés au component menu-logo

* Il ya certain components qui sont necessaire pour le fonctionnement d'un module ou des components d'un module 

et il y a des components qui sont utilisables a l'exterieur.

**Comment ajouter un module Angular manquant ?**

Clic sur l'élèment surligné en jaune (en occurence routerlink dans l'ex ) ,  selectionner le module, creer précedement et qui va être proposé par Angular.

=> Importation automatique du module manquant dans le dossier **app.component.htlm** le module **MainMenuModule** ce qui va le rendre disponible dans 

d'autres modules afin de pouvoir s'en servir dans htlm.

///video117///
