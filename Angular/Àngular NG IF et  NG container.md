## Angular NG, IF et NG container

[NG IF] permet de faire un if mais dans l'htlm

ex:

On veut que quelque chose s'affiche uniquement si l'utilisateur a plus de 32 ans

**Dans menu1.component.ts**

On va creer une var age :


name: string;

age = 32,

allRoutes: MenuModel [] = [

{url: 'home', name: 'accueil'},

{url: 'à-propos', name: 'à propos'}];

-----------------------------------------------------------------
**Dans menu1.component.htlm**

<app-menu-logo></app-menu-logo>

<ul>

Bonjour, {{sayHello().age}} {{sayHello().name}}

<h2 *ngIf="age > = 32 >INFO POUR LES VIEUX</h2>

<li *ngFor="let current of allRoutes">

<a [routelink]= "current.url">{{current.name}}</a>

</li>

</ul>

Supposont qu'on veuille que le lien a propos ne sois visible que par les personnes qui ont 32 ans

**Dans menu1.component.ts**
````
name: string;

allRoutes: any [] = [

{url: 'home', name: 'accueil', minimunAge:0},

{url: 'à-propos', name: 'à propos', minimumAge: 32 }];
````

Ensuite il faut aller rajouter minimumAge dans le type model.

````
Ex:

export interface MenuModel {

 url : string;
 
name : string;

minimumAge: number;

}
````
Si le lien est reserver au personne de plus de 32 ans uniquement

**Dans menu1.component.htlm**
````
<app-menu-logo></app-menu-logo>

<ul>

Bonjour, {{sayHello().age}} {{sayHello().name}}



<li *ngFor="let current of allRoutes">

<a
 
 *ngIf="age >current.minimumAge
 
 [routerLink]= "current.url">
 
 {{current.name}}
 
 </a>



</li>

</ul>
````
On ne peut mettre qu'une seule directive structurel [*]

[NG CONTAINER]: 

Quand on veut mixer des directives entre elles , il faut utiliser  [NG CONTAINER]

````
<ng-container *ngFor= "let current of allRoutes">

<li *ngFor="let current of allRoutes">

<a
 
 [routerLink]= "current.url">
 
 {{current.name}}
 
 </a>

</li>

</ng-container>

</ul>

````

///video122///
