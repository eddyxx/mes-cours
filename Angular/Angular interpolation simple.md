## Angular interpolation simple

On declare des variables en tant que propriétés dans le component, on leur met un type sois implicite sois explicite

**qu'est ce que interpolation du htlm ?**

htlm peut calculer des choses qui viennent du component

l'interpolation se fait avec des doubles accolades {{}}:

````
ex;

<app-menu-logo></app-menu-logo>

<ul>

bonjour, {{name}}

<li>

<a routerlink ='home'>home</a>

</li>

<li>

<a routerlink="à-propos">à-propos</a>

</li>

</ul>
````

**Ensuite aller dans menu1.component.ts et declarer name :**

* Typage implicite:

````
=> name = 'jean-michel';
````
* Typage explicite:

````
=> name: string;

constructor() {

}

ngOnInit(): void {

this.name = 'jean michel';

````
[routeroutlet] =>  Permet de connaitre l'endroit ou l'on navigue.


///video118///
