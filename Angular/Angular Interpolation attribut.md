## Angular Interpolation attribut

[router link] est une propriété gerer par angular

Quand il n'y a pas de double symbole d'une interpolation [{{}}], Angular considere qu il s'agit d'un string.

### A. Comment aller chercher la valeur d'une prop et non un string (interpolation sur une prop) ?

On va créer des var dans le fichier 

**Dans le fichier menu1.component.ts**

link1 = "home";

link2 = "à-propos";


**Dans le fichier menu1.component.htlm**
````
<app-menu-logo></app-menu-logo>

<ul>

Bonjour, {{sayHello  |  json }}

<li>

<a[routerlink] ="link1">home</a>

</li>

<li>

<a [routerlink] ="link2">à-propos</a>

</li>

</ul>
````
* Permet d'activer link 1 et link 2

 Les interpellation d'attribut doit se faire avec les crochets au nom de l'attribut !!!

///video120///
