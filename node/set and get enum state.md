## Set and Get enum state

On utilise les enums pour gérer les differents états [state] d'un objet.

**astuce:**

 * enums = 
 
 il s'agit d'un objet dans lequel il y a une clef |=> 'isAvailable' 

aisni que la valeur a l'interieur |=> 'est libre'

````
ex:

export enum RoomState {

isAvailable = 'est libre',

isCleaning = 'en nettoyage',

isOccupied = 'occupée',

isClosed  = 'en travaux',

````

**verification du [state]** 

On fait une fonction qui va verifier que c'est bien un vrai state.

````
function checkRoomState(state: any) {

return Object.keys(Roomstate).some((roomState: string)=> roomState === state);

````
Astuces:

 -**Sur postman :**
 
_[clic sur params]_

quand on met 2 pts avant le slash ,le logiciel va creer automatiquement, une pass variable
 
=> variable qui est dans le chemin (second tableau sur postman) et met auto le nom qu'il ya avant les 2 petits pts.

````
Memo:

- alt + entrer = import fichier
````

///video72///
