## Les enums


#### A. Qu'est ce qu'un enum ?

Une énumération se définit à l’aide du mot-clé [enum] suivi du nom de l’énumération et de ses membres...

ex:

````
export interface RoomModel { 

  id: number:
  
  size: number:
  
  roomName:: string:

}

export enum RoomState {

isAvailable = 'isAvalaible',

isCleaning = 'isCleaning',

isoccupied = 'isoccupied'

isClosed = 'isClosed',

}

const room: RoomModel = 

id: 1,

roomName: 'suite lulu',

size: 12, 

roomState: roomState.isAvailable => on manip seulement cet élément dans tout le code.

console.log(room)
````

///video71///
