## Principe simple de firebase

Quand on fait du node , on fait de l'admin

dans la partie documentation de firebase

=> selectionner get started for Admin .

=> cloud firebase

=> get started for admin 

=> npm install firebase-admin --save

- donne l'indication au systeme que je veut installer firebase admin dans mon projet 

#### package.json

(Dans package.json = il y a toutes les descritions de toutes les app installer quand on fait npm i)

* [ts lint] = un linter verifie la qualité de mon code

* [express] = gére les get et les post 

*[ pm2 ]= moniteur qui permet de relancer le serveur quand il plante.

* [@types/ ] = tout les élèments qui commence par @types permettent a typesript de reconnaitre les informations de typage des programmes qui sont indiquer a droite  

```` 
 ex: 

 Dans package.json 
 
*  @types/node => 
 
*[ Les informations de typage] = interfaces liée aux programmes cors , express, node.
````

 - Sur le site de firebase: 

=> get started 

=> copier/coller dans server.ts (webstorm) 
 
 ````
 => admin.initializeApp({
 
        credential: admin.credential.applicationDefault()
    
     });
     
````

#### Memo

- Quand on veut selectionner un hotel en particulier par id depuis firebase;

['GHua3CzNgNLPjhJhyTA1'] c'est la ref qui permet de savoir ou se trouve l'hotel /comme une adresse postal

=> const ref = db.collection('hotels').doc('GHua3CzNgNLPjhJhyTA1');

- Quand on veut toute les collections de tout les hotels:

=> const ref = db.collection('hotels');

### synchrone/asynchrone

[synchrone] = Se dit des mouvements qui se font dans un même temps..

[asynchrone] = se dit des mouvements qui se font l'un aprés l'autre ...

Il y a plusieurs étapes dans le temps qui se deroulent ; 

=> je vais chercher mes données dans firebase (service heberger dans le cloud)

- Le serveur va  prendre ma collection d'hotel et lorsque il y aura [get] /api/hotel , 

- Le serveur va me les renvoyer aprés être aller les chercher dans ce cas on ajoute [async] au code

````
ex:

app.get('/api/hostels/',async (req, res) => {

    try {

        const hotels = await ref.get();

        return res.send(hotels);

    }catch (e) {

        return res.status(500).send({'erreur serveur:' + e.message})

    }

});

* async est devant la fonction de callback pour expliquer que c'est une fonction de callback qui va pouvoir 

faire des etapes de facon temporel.  

[get] veut dire va me chercher ce qu'il y a dans la reference 

[await] = attend d'avoir le resulta 

ne pas mettre await avec les operateurs (for , reduce , foreach ...)

````
[CTRL + GET] = promesse qu'on va avoir une réponse de querrys (interrogation de la base de donnée) snapshop (une capture et une date [T]= on veut quelque chose dont on ignore le type )


