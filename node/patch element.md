## Patch element

- Sur postman :

* mettre en patch 

{
'roomNumbers':10000
}

=> puis send 

cela permet de changer l'attribut sans toucher a hotel ocean.

Si je met un element inexistant , le patch va ajouter le nouvel element dans la database firebase. 

Quel est la difference entre [patch] et [put] ?

- La méthode [PATCH] d'une requête HTTP applique des modifications partielles à une ressource.

[PATCH ] n'est pas listée comme étant idempotent, contrairement à PUT. Cela signifie que les requêtes patch identiques et successives auront des effets différents sur l'objet manipulé.

- La méthode [ PUT] est déjà définie pour écraser une ressource avec un nouveau corps complet de message.
