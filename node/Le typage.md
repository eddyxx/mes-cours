## 10. Le typage

Le typage = precise de quel type sont les éléments qu'on va manipuler => entier, number, string , ect...

On va creer des dossiers "modéle" (c'est une norme) ensuite on va creer des fichiers interface:


````
ex;



=> company.modéle.ts

import {UserModel}

export interface companyModel {

}

ex;

La company detient des users

- Dans le dossier creer => user.model.ts

export interface UserModel {

id: number,

lastname: string;

age: number;
}


````
- Il faut toujours mettre une majuscule a une interface

_Il n'y a jamais de S (fichier au singulier car c'est 1 interface pour 1 seule et unique utilisateur)_

- [export] rend accessible a l'exterieur

 - 0n fini toujour une interface par un point virgule
 

### La valeur de retour d'une fonction:

Quand on creer une fonction qui passe en parametre un Id

````
function getUserId(id:number) :UserModel | undefine {

return user.find(predicate(user:UserModele)=>user.id=== id)

}

console.log(getUserById(id:2)) 

=> la console va afficher le user 2
````

#### Memo

 * (id:number ) => car dans le fichier user.modéle ,il y a  une valeur numérique avec id .

* | => veut dire 'ou '
