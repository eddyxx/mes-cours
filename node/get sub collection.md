## Get sub collection:

#### 1. comment avoir l'integralité des infos contenue dans l'hotel ainsi que l'integralité des infos contenue dans les rooms quand je fais un [get]**

On utilise dans ce cas [promise.all]:

=> prendre le app.get de id

* on va creer une variable => [const hostelTofind] qui va être l'hotel a trouver

=> On va utiliser cet variable en applant une fonction nouvel qui va aller chercher un hotel dans un 1er tps puis ensuite toutes les chambres qu'il y a à l'interieur de l'hotel .

=> nommer la fonction en export puis ajouter async (car l'action se joue en 2 tps)

qui  prend en param un hotelId et qui va  renvoyé une promesse .

  **a. Comment verifier que l'hotel existe :**

=> const hoteltofind: hotelModel = await getHostelById(hostelId)

* ensuite on creer une nouvelle const:

- const roomref (les ref de la room de l'hotel) = c'est une collection réference et qui

nous permet de  faire un [promise.all] = pour aller chercher toutes les chambres.

- const roomref = refHotels.doc(hostelId).collection(collectionPath 'rooms');

**[Quand on get une collection , il faut faire un foreach ()**

- const roomsref = [] => (tableau vide au début de l'obejt [partie qui contien la ref et l'id de la chambre])

- const roomsref: string[]=[];

roomsCollectionInhotelSnap.foreach(roomref =< roomsref.push (roomref.data(.uid));

**Function qui permet de renvoyer toutes les rooms qui correspondent lorsqu'on va envoyer des uid de string**

_(dans room.service.ts)_

export async function getAllRoomsByUids(roomsUids: string[]) {

const promisesToResolve: promise<RoomModel>[] = roomsUids

.map(Uid => getRoomById(Uid))

return await promise.all(promisesToResolve);

}

[ si la room n'existe pas , la function nous renvoie un message d'erreur.]

**b .Comment avoir des rooms qui vont être égale a (getallroomsbyid)?**

-(dans hotel.service.ts)_

* const rooms =await getallRoomsByuid(roomsref);

return hotelTofind.roomsData= rooms;

return hotelTofind

_(dans server.ts)_

app.get( '/api/hostels/:id', async (req, res)=>  {

try {

const hostelId: string =req.params.id;

const hostelTofind: HotelModel = await getHostelByIdwithallRooms(hostelId)

}

