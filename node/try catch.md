## Try catch


* [Try catch] = L'instruction try...catch regroupe des instructions à exécuter et définit une réponse si l'une de ces instructions provoque une exception.

_L'humain va prendre en charge l'erreur et faire en sorte que cet erreur ne fasse pas exploser le systeme_.


````
ex:

si dans les serveurs qui vont heberger les applications , il y a du mauvais code , le server va planté

cela va permettre au systeme d'anticipé une action en cas de crash du serveur

Pour eviter de faire planter un serveur on englobe les operations qu'il fait dans un try catch car ca permet 

d'eviter de faire planté le systeme

ex:

try {

  nonExistentFunction();
  
} catch (error) {

  console.error(error);
  
  // expected output: ReferenceError: nonExistentFunction is not defined
  
  // Note - error messages will vary depending on browser
}
````

=> try defini la tache a exécuter.
 
=> je vais catcher (e) et puis je vais recuperer l'erreur.

=> l'erreur a un texte , il faut aller dans e.message et ensuite [return] res (retourner la réponse).

=> signale l'erreur 

= .status (code:500) => [code 500 = le serveur a planté]

=>  renvoi le message  'error server' 

= .send(message:{'error server' + e.message});

_voir ci dessous_

````
app.get ('/api', (req, res)=> {

try {                 

return res.send(data 'hello world2');

} catch (e) {

console.error (e.message)

return res

.status(code:500) => 

.send(message:{error: 'erreur server:' + e.message});

}

)};
````

