##  9. Les fonctions :



Une fonction est comme une machine qui va prendre en entrer des infos et qui va les traiter, 

en son sein (O)  et qui va fabriquer une sortie 
````
                   =====> O =====>
````
function my fonction (a,b) {     => a et b sont les paramétres 

console.log(a + b)

}
=> Affiche NaN car les parametre sont indefinies:

function add(a, b) {

return a + b;

}
console.log(add(a:2, b:3))

             
  => Affiche le resulta sois 5 car il s'agit d'une addition numérique.

- _Les resultats s'affichent sur la console de serve et non sur la console de sur server localhost._

Dans le cas d'une fonction d'addition :

````
**Methode 1:**

function add(a, b){

return a + b;

}

**Methode 2: (arrow function)**

- const result =add(a:3, b:4);

- const addbis = (a, b) =>a + b ;

- const resultBis = addBis (a:3, b:4);

console.log(result, resultBis);
````



**Memo**

_addbis ne doit pas être aprés  resultBis_

_On n'a pas le droit de declarer une const avant sa creation_
