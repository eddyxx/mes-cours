## truthy et falsy 

_Sur console de la page en ligne_

Falsy = toute les valeurs qui sont fausses

truthy = toute valeurs qui sont vrai 



#### comment on verifie si une valeur est vrai ou fausse ?

````
=> if () {console.log( 'ok')}
````

* Si [truthy] on verra apparaitre 'ok' si [falsy] , 'ok' n'apparaitra pas

* [!] signifie l'inverse

````
=> if (!true) {console.log('ok')}
````

* La console log n'affiche pas 'ok' puisque l'inverse est false

````
=> if (!false) {console.log('ok')}
````

* La console affiche 'ok' car l'inverse de false est true

#### teste avec nombres:


````
=> if (! [2, 3] ) {console.log('ok')}
````
* La console affiche indefined donc rien car l'inverse de quelque chose qui existe , n'est donc pas definie car il n'existe pas.
  
  _pour les nombres on cherche a savoir si le resulta existe ou pas_ 
  
- si je veut verifier qu'une chose existe et que la reponse sois un boleen [true/false]
  
  je rajoute 2 point d'exclamation ce qui transforme en boleen [true/false]
  
 ```` 
 => if (!!indefined) {console.log('ok')}
 ````

* La console affiche false puis que l'inverse d'indefini est truthy , l'inverse de truthy en valeur boléenne est false.


 



