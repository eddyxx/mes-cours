### Promise.all

#### **1. Qu'est ce que promise.all ?**


La méthode [Promise.all(]) renvoie une promesse (Promise) qui est résolue lorsque l'ensemble des promesses contenues dans l'itérable passé en argument ont été résolues ou qui échoue avec la raison de la première promesse qui échoue au sein de l'itérable.

* _(Un objet itérable (tel qu'un tableau (Array)) contenant des promesses)_.


**[promise. all]** permet d'envoyer toutes les promesses en même tps 

=> Pour recevoir toutes les réponses plus ou moin en même tps  _(le plus vite possible)_.

Quand on va declencher [promise.all] , les actions exécuter seront async car promise all va  

=> Donc il faut ajouter [await] avant ...


_(ex = video 67)_
