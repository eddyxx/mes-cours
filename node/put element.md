## Put element

[put] remplace l'integralité des choses qu'il y a l'interieur 

de l'objet par le nouvel objet qu on lui place.


On va creer une fonction pour verifier que l'element existe avant de [put];

````
ex:

[put] = [set]

export async  function putHostel(hostelId: string, newHostel: hotelModel): HotelModel {

if (!newHotel || !hostelid) {

throw new error ( 'hostel data and hostel id must be filled');
  }
const hotelToputref: documentreference = await testifhotelexistsbyid(hostelid);

await hotelToputref.set(newHostel);

return newHostel;

}
````
