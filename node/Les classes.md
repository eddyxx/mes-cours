## Les Classes:

#### A. qu'est ce qu'une classe?

Une classe est un outil developper aprés le demarrage de l'informatique pour aider les dev a conceptualiser des choses compliquées a faire en terme de code .

Une classe est une représentation abstraite et non pas la représentation particulière d'un membre de cet ensemble d'objets. Par exemple, la classe Employé permettrait de représenter l’ensemble de tous les employés.

est tres utile a partir du moment ou on utilise l'outil principal qui est [l'heritage]

**une classe se declare :**

````
export class HotelClass {

id?: number;

name?: string;

roomNumbers?: number;

pool?: boolean;

isValidated?: boleen;

     

constructor (

             id?: number,
             
             name?: string,
             
             roomNumbers?: number,
             
             pool?: boolean,
)

}
````
[constructor ()] =>  fonction dont le nom est imposé et qui est destiné a creer une instance d'une classe.

#### B. qu'est ce qu'une instance ?

 Une instance correspond à l’instanciation d'une classe. 

C’est un de ses membres. Ainsi, Victoria pourrait être une instance de la classe Employé et représenterait un individu en particulier comme un employé. 

Une instance possède exactement les mêmes propriétés que sa classe (ni plus ni moins).

**Astuces:**

=> Seconde Methode:
 
constructor(hotel: HotelModel) {

Obeject.assign(this, hotel)

}

**On utilise cet methode quand il y a trop d'argument (plus de 4/5 arguments)**


#### C. Comment re-calculer la valeur des rooms (objet) ?:

````
calculetRoomNumbers() {

this.roomNumbers = this.rooms.length

}
````

**Memo:**

* [Super()] => fonction qui appel le constructeur de la class parents 

* [implements] => force a avoir le même modéle 
