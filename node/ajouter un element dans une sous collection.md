### A . Comment ajouter un élément dans une sous collection ?

**1. Comment ajouter un objet ?**


Faire une route qui va dans l'hotel et poster une chambre => je creer une nouvelle chambre et je vais l'attacher a un hotel.

**admin.initializeapp** doit être fait 1 seule fois 

````
import admin from 'firebase-admin';

import {RoomModel} from '../models/room.model'

const db = admin.firestore();

export async function createRoom(hotelId: string, newRoom: RoomModel): promise <string> {

if (!hotelid || !newRoom) {

throw new Error( 'hotelid or newroom are missing' )

}

return 'ok';

{
````

=> **async function** prend en param un hotel id qui est un string et une nouvelle room de type roomModel.


**2 .Comment verifier que l'hotel existe ?**

````
import admin from 'firebase-admin';

import {RoomModel} from '../models/room.model'

const db = admin.firestore();

const hotelref = collectionreference = db.collection (collection path 'hotels');

const roomref = collectionreference = db.collection (collection path 'Rooms');

export async function createRoom(hotelId: string, newRoom: RoomModel): promise <string> {

if (!hotelid || !newRoom) {

throw new Error( 'hotelid or newroom are missing' )

}

const hotelSnap: Documentsnapshot = hotelref.doc(hotelid).get();

const hoteltofind: HotelModel = hotelSnap.data();

if (!hotelSnap.exists) {

throw new Error('hotel does not exists' )

}

return 'ok';

{
````

**Pour tester son code on va sur postman :**

=> raw

=> test = Json

=> post

_(video 66)_
