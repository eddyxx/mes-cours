## Query param

#### a. Comment passer des param de query a une route qu'on a creer [path]?:**

on va prendre un api que l'on va appeler api/testquery

app('/api/testquery', async (req, res)=> {
try {
return res.send ('coucou')

} catch (e) { 

return res.status(code:500).send(message:{error:'error serveur:' + e.message})

}

});

app.get('/api/hostels', async (req, res)=> {

try {

   const hostels = await getAllHostels();
   
   return res.send(hostels);
   
 } catch (e) { 

return res.status(code:500).send(message: {error serveur :'+e.message});

  }
  
});

````
verifier sur http://localhost:3015/api/!

=> result 'coucou' sur la page web
````

Quand on fait une requête = [query]

il est possible de specifier les param de query = 

#### b.Comment faire ?

 On ajoute [?], ensuite on met le nom de la query souhaité.

par ex :

**localhost:3015/api/testquery?limit=20**

on va creer un objet  const  [limit] qui va nous permettre de recuperer  :

_(dans app.get de api/testquery)_

const limit = req.query.limit

Le systeme va recup ce qui a été ajouter sur l'adresse du server (localhost:3015/api/testquery?[limit=20])

et il va le stocker dans [req.limit]


_(les query permettent de passer que des strings)_

**[&] = le 'et' commerciale => permet de dire que l'on va mettre un second argument.**

_(En general les query ne sont que pour les [get])_

///video 69///
