### Les sous collections


**Qu'est ce qu'une sous collection ?**


Il s'agit d'une collection contenue dans une autre collection qui seront liés par un id, une reference.
 

 **ex:**
```` 
 
Dans un hotel , une nouvelle collection (_sous collection_) va être creer dans laquel il va y avoir une collection room contenant des infos sur les chambres de l'hotel 

Dans une sous collection on ne met pas l'integralité de l'objet mais seulement une reference vers l'objet.

````

* Dans une base de donnée , les hotels sont separer des chambres

tout comme une equipe de foot peut être séparer des joueurs, ect....

ex:

Dans la collection hotels/equipe de foot on va retrouver tout les hotels/toutes les equipes de foot , dans la collection room/players , on va retrouver toute les chambres/tout les joueurs.

Dans la sous collection room/player ou hotel/equipe,je peut rajouter l'id et la reference de la room/player ou de l'hotel/equipe .

