##  Array operateur foreach;


tab.foreach(user) => on va parcourir tout les elements d'un tableau.

=> on va parcourir les users un par un

````
ce qui donnera console.log('user:', user))
````

=> on va avoir une fonction de callback

Pour le rendre disponible dans la fonction 

- cet fonction peut nous envoyer 2 elements

````
=> tab.foreach ((user:{...}|...,idex:number)=>{

console.log ('user; ',index, user);
 })
 ````

. Ou en utilisant des `"string template: 

````
=> tab.foreach ((user:{...}|...,idex:number)=>{

   console.log ('l'user dont l'index est $ {index}; s'appel {user.firstname} ');
````
