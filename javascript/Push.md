### Push ()

[push ()] permet de réinitialiser les valeurs d'un tableau

* Notez que [push()] ne retourne pas de nouveau tableau, mais seulement la nouvelle taille du tableau. C'est le tableau d'origine qui est directement modifié par l'appel.

* Quand on veut rajouter des elements dans un tableau => [.push]

const tab2 = []; => creation d'un nouveau tableau..

const baby1 = {

name: 'basile'

};

const baby2 = {

name: saka

};

tab2.push (baby1)

tab2.push (baby2)

console.table(tab2)
