## qu'est ce que javascript?


Javascript est un langage de programmation, c'est une forme de code qui permet, quand on sait l'écrire, de dicter à l'ordinateur quoi faire. C'est du texte. ... En effet, c'est le seul langage qui permette de dicter à un navigateur Web (Internet Explorer, Firefox, Chrome…) ce qu'il doit faire sans rien installer.

JavaScript est un langage de programmation qui permet de créer du contenu mis à jour de façon dynamique, de contrôler le contenu multimédia, d'animer des images, et tout ce à quoi on peut penser. Bon, peut-être pas tout, mais vous pouvez faire bien des choses avec quelques lignes de JavaScript.

### a. Qu'est-ce qu'une fonction en JavaScript ?

Pour être tout à fait précis, les fonctions prédéfinies en JavaScript sont des méthodes. Une méthode est tout simplement le nom donné à une fonction définie au sein d'un objet.

### b. Pourquoi coder en JavaScript ?


Le JavaScript (à ne pas confondre avec le Java :)) est un langage de Script orienté objet inventé en 1995 ! Ce langage de programmation historique dans le monde du web, permet de créer des sites interactifs (coté front), des applications mobiles (iOS et Android) et des applications côté serveur extrêmement réactives !


### c. Définition du mot Script


En informatique, un script désigne un programme (ou un bout de programme) chargé d'exécuter une action pré-définie quand un utilisateur réalise une action ou qu'une page web est en cours d'affichage sur un écran.


#### * L'HTML est un langage informatique utilisé sur l'internet. Ce langage est utilisé pour créer des pages web. 


L'acronyme signifie HyperText Markup Language, ce qui signifie en français "langage de balisage d'hypertexte". ... Par exemple le CSS, qui permet de mettre en forme le contenu d'une page codé en HTML.

Un framework (ou infrastructure logicielle en français ) désigne en programmation informatique un ensemble d'outils et de composants logiciels à la base d'un logiciel ou d'une application. ... L'objectif du framework est de simplifier et d'uniformiser le travail des développeurs.


#### * PHP (officiellement, ce sigle est un acronyme récursif pour PHP Hypertext Preprocessor) est un langage de scripts généraliste et Open Source, spécialement conçu pour le développement d'applications web. Il peut être intégré facilement au HTML.

Comment deboguer un script ,Pour vous débarasser à tout jamais de ces erreurs, rendez-vous dans le menu Outils/Options internet, et cliquez sur l'onglet "Avancé". Retirez la coche à "Afficher une notification de chaque erreur de script", et cochez "Désactiver le débogueur de script". Cliquez sur OK. C'est tout.


