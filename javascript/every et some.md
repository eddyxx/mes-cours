## Every / Some


[every/some] = operateurs qui  renvoi un booléén :



- [ every ] :

Si tout les elements d'un tableau repondent a un predicate il va me renvoyer [true] sur la console.

````
=>const allHostelpool = hotels.every((hotel)=> hostel.pool=== true)

  console.log(allHostelpool);
 ````
Dans le cas ou un seule hotel ou plus , dans la liste d'hotel, ne posséde pas de piscine

sur la console on verra s'afficher false.
 
 ````
 const allHostelpool = hotels.every((hotel)=> hostel.pool=== false)

````

- [ some ] :

Si il n'y a qu'une seule et unique piscine parmis tout les hostels , some  renvoi true

````
 const allHostelpool = hotels.some((hotel)=> hostel.pool=== false)
````

si il n'y a aucune piscine parmis tout les hostel , some  renvoi false

````
=> const allHostelpool = hotels.some((hotel)=> hostel.pool=== false)
````
