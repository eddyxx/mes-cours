## Array operateur find;

j'utilise find () qui va me retourner un predicate , donc une valeurs boleenne

- Je dois savoir si un hotel a bien une piscine :

````
* const HoTelTofind= hostels.find((hotel)=> hotel.pool=== true);
````

- Je cherche un hotel qui s'appel [hotel ocean]:

````
const HotelTofind = hostels.find((hotel)=> hotel.name=== 'hotel ocean')
````
* Find renvoi un objet donc on ne peut pas enchainer find avec un autre operateur .
