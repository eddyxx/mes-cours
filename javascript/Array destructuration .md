## Array  destructuration


La destructuration permet de récuperer des variables dans un tableau , afin de leurs donner un nom directement 

et d'y avoir accés  de maniere plus esthétique ;



````
=> const tab= [ 'tata','toti', 'lola', 'zazi'];

tab[1] = 'coucou';

 const var1 = tab[0];
 const var3 = tab[2];
 
 * aprés destructuration : 
 
 => const [var1, var2, var3, var4] = tab;
 
````
