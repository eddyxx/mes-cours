## slice vs splice:

*  slice();

extrait une section d'une chaine de caractères et la retourne comme une nouvelle chaine de caractères. 

La chaîne de caractères courante n'est pas modifiée.

````
ex :

const str = 'The quick brown fox jumps over the lazy dog.';

console.log(str.slice(31));
// expected output: "the lazy dog."

console.log(str.slice(4, 19));
// expected output: "quick brown fox"

console.log(str.slice(-4));
// expected output: "dog."

console.log(str.slice(-9, -5));
// expected output: "lazy"
````

*  splice();

Modifie le contenu d'un tableau en retirant des éléments et/ou en ajoutant de nouveaux éléments à même le tableau.

On peut ainsi vider ou remplacer une partie d'un tableau.

````
* 0n veut ajouter un  élèment:

const months = ['Jan', 'March', 'April', 'June'];

months.splice(1, 0, 'Feb');


console.log(months);

// expected output: Array ["Jan", "Feb", "March", "April", "June"]

months.splice(4, 1, 'May');

// on veut remplacer un element a index 4

console.log(months);

// expected output: Array ["Jan", "Feb", "March", "April", "May"]
````
