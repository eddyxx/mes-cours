## Arrow functions vs functions

````
code 1 :

function testFunction(a, b ) {
return a + b;
}
````

- Ecriture abreger  avec Arrow function =>

````
code 2 : 
 
 const testFunction2(a, b)=> {return a + b}
 
 * console.log(testFunction(a:2, b:3)); = code 1
 
 * console.log(testFunction2(a:2 , b:3)); = code 2
 ````
 
- [=>]= arrow fonction qui est la version abreger du code 1

- ps: pour la declarer il faut faire [const]
