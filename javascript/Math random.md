## Math random

[Math.random ] permet de creer des nombres aléatoire

- l'aleatoire en informatique n'existe pas , on utilise la memoire vive de l'ordi + l'horloge interne du micro processeur 
pour determiner un nombre = pseudo aleatoire puisque la proba pour que 2 pc generent les mêmes chiffres est forte.

- Pour generer un nombre aléatoire :

````
=> console.log(Math.random());
````

* console result => c'est toujours un nombre compris entre 0 et 1 (sans jamais atteindre 1)

````
=>  console.log(Math.random()* 1000 + 1);
````
- la console affiche un nombre compris entre 1 et 1000


- si je veut enlever les nombres aprés la virgule:

````
=> console.log(Math.floor(x:Math.random() * 1000 + 1));
````

### Random entre 2 nombres ;

C'est une fonction baser sur Math random

On veut obtenir un nombre entre 0 et 1 et je vais ajouter un min et je vais le multiplier par la difference du min et le max

````
ex
function createRandomNumber(min, max){

return Math.floor(x:min + Math.random()* (max-min) + 1);

}
````
=> je vais avoir un chiffre compris entre 10 et 15 generer en aleatoire.

````
console.log(createRandomNumber(10, 15));
````

