## Destructuration des objets :

Quand il y a un objet compliqué et que je recupére plusieurs elements alors que je souhaite travailler qu' avec 1 ou 2  elements en particulier

il faut destructurer l'objet :

````
ex:

const user = {

firstname: 'bob',

lastname: 'gueli',

age: 34,

bilionaire: false,

eyes: 'blue',

}

- Version initiale :

const age = user.age ;

const lastname = user.lastname;




- Aprés destructuration de l'objet : (_plus esthétique_)

const {age, lastname} = user;

console.log()

````
