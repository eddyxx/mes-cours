## Object as list

permet de mettre dans un objet , d'autre objet dont les clefs decrivent des listes .

Au lieu de mettre des index , on a des attributs de clef qui permettent de délimité les objets.


````
ci dessous hotels est un objet et la premiere clef d'hostel (attribut) est [id]

const hostels = {

hotels:

id: 1

name: 'hotel les pins'

roomNumbers: 10,

pool: true, 

rooms:

{...},

{
room1: {

roomName: 'suite de luxe',

size: 2,

id: 1,

{

room2: {

roomName: 'suite nuptiale',

size: 2,

id: 2,

{
````

=> {...} contient la liste ci dessus.
