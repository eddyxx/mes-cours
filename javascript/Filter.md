## Filter ;

=> filter sert a filtrer un tableau par élément 


````
ex:
   je veut retirer les gens qui ont plus de 30 ans mon tableau

const filteredArray = tab.filter(user => user.age >30);

console.table(filteredArray);

On peut combiner plusieurs operateurs 



quand on fait des fonctions que l'on enchaine sur des operateurs


const filteredArray = tab

.filter(user => user.age >30);

.map((user)=> {

user.lastname = user.lastname + 's';

return user;

console.table(filteredArray);


}) 

OU 

=> _La programmation fonctionnelle_:

const filteredArray = tab

.filter (isUnder30)

.map(addSatEnd)
;

function isUnder30(user) {

return user.age <30

}

function isOver (user) {

return user.age > 30

}
````

