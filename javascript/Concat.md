## concat:

La méthode concat() est utilisée afin de fusionner un ou plusieurs tableaux en les concaténant.
 
Cette méthode ne modifie pas les tableaux existants, elle renvoie un nouveau tableau qui est le résultat de l'opération.


````
const array1 = ['a', 'b', 'c'];
const array2 = ['d', 'e', 'f'];
const array3 = array1.concat(array2);

console.log(array3);

* run console => Array ["a", "b", "c", "d", "e", "f"]

=> Operateur concatenation :

const tab2 = [1, 2, 3];

const tab3 = [4, 5, 6];

const tab4= [...tab2,...tab3];

console.log(tab4);

 -Trois point pour copier un objet ou un tableau et ça enléve les crochets et parenthéses ...
````
