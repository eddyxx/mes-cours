## React redux

### A. Redux ?

(_redux est une librairie qui peut être  utilisée avec d'autre language_)

Il n'y a pas de projet react sans redux ou un equivalent.

_Les données sont supposées être immuables ,_(Qui reste identique, ne change pas)_.


* [un store] =  endroit ou on va stocker toutes les props d'une application.

le store va recuperer l'information que la props a changer et va dispatcher cet info a toute l'application.

* [Todo] = liste a faire



**Pour faire une To do liste on va:**

 * Faire des reduxer => systemes qui vont nous permettre de recup les infos

 * Creer des actions => changer des infos
 
 * Faire des selecter => permet de selectionner une info voulu

 * Creer le stores .
 


Il faut definir les types d'action , dans cet exemple :

ADD_TODO = ajouter un Todo

TOGGLE_TODO=  action quand le user  clic sur le button

-----------------------------------------------------------------
**Dans actionType.js**

````
* On commence par definir 2 constantes qui vont nous permettre d'identifier les 2 actions.

import {ADD_TODO, TOGGLE_TODO} from "../actionTypes";

const initialSlate = 

 allIds: [],
 
 byIds:{},
 
 };
````

**Dans todo.js (dans la structure de l'application)**


 * (  l'objet 1 ou 2 representent les todo.)
   
   * a l'interieur de la base de donnée on remet l'id dans l'objet.  
   
ci dessous, on n'a une fonction qui prends en param l'état ainsi que l'action a effectuer.

En fonction de l'action demander on va retourner la data qu'on voudra avoir.

[...State] => copie de l'objet = respect du principe d'immutabilité.


````
import {ADD_TODO, TOGGLE_TODO} from "../actionTypes";

const initialSlate = 

 allIds: [],
 
 byIds:{1:{completed: true, id: 1}, 2 : {}},

 };
 
 export const todos = (State = initialState, action) => { 
 
 switch (action.type) 
 
 case ADD_TODO: {
 
 const {id, content} = action.payload;
 
 return {
 
 ...state, 
 
    allIds: {...state.allIds, id},
    
    [id]: {
    
      content,
      
      completed:false
           
           }
        
        }
     
     }
   
   }
  ```` 
  
     
**Dans action.js**


````
import {ADD_TODO, TOGGLE_TODO} from "../actionTypes";

    let nextTodoId = 0;

    export const addTodo = content => {

       return {

       type: ADD_TODO, 

        playload: {

        id: ++nextTodoId,

         content,
    
        }
  
      }

     };
     
     export const toggleTodo = id => (
     
     type: TOGGLE_TODO,
     
     payload:{
     
          id
     
        }
        
     });

````

[content] on va recup les infos qui sont a l'interieur du Todo.

[type] => type d'action possible (add todo ou toggle todo)

[payload] data qui vont être envoyé et qui seront necessaires pour réaliser l'action.
_(12min)_

[++nextTodoId] => quand je veut ajouter un todo , il faut creer une nouvelle Id et je vais faire ++nextTodoId car le point de dêpart est 0 

**let nextTodoId = 0;**

**en mettant ++ devant , l'operartion se fait en premier.**

-------------------------------------------------------

**Dans le fichier selector.js**

````
export const getTodosState = store => store.todos;

=> Dans le store ou est stocker toute les datas de l'appli ,on va recup tout les todos

export const getTodoList = store =>

getTodoState(store) ? {...getTodosState(store).allIds :[]};

=> On va renvoyer ds getTodo, la liste des Id sinon on renvoi un tab vide.

export const getTodoById = (store, id) =>

getTodoState(store) ? {...getTodosState(store).byIds[id], id}:

 => on va verifier qu'il y a bien des todos , et si c'est le cas on va aller chercher dans la liste des todos l'id qu'on veut avoir (en supplément).

export const getTodoList = store =>

getTodoState(store).map(id => getTodosById(store, id))

 =>  pour avoir tout les todos , on ft getTodosState(store).map pour voir les ids de chaque Todoliste.

  ````

### B. Comment initialiser le store ?

**Dans le fichier store.js**

import {createStore} from 'redux';

import rootReducer from "./reducers";

export default createStore(rootReducer)

-------------------------------------------------------------------------------------->

Pour que ça puisse marcher , il faut avoir fait const Todo dans le fichier [todo.js]

Puis dans le fichier index.js on va faire un import de todos , de combineReducer depuis redux et un  export default qui va 

[combinereducer ]est un operateur qui vient de reduce et qui permet de combiner plusieurs reducer

**ex ci-dessous:**

````
import {combineReducers} from 'reduce';

import {todos} from "./todos";

export default combineReducers(reducers:{todos});
````
[combineReducers] = prepare les reducers dans le store et les activer

il faut alimenter l'appli avec le store pour qu elle puisse marcher.

[provider] est un component donner par react reduce

[connect ] il va connecter le component au store 

il va prendre un premier param qui va être 

[mapStateToProps] on va changer l'etat de l'appli en props


_Quand il n'y a pas de props par default , on met Null_

### C. Comment avoir accés a la liste des todo depuis le fichier store.js

const mapStateToProps = state => {

const todos = getTodos(state);

console.log(todos)

return {todos}

};

export default connect(mapStateToProps)(TodoList);

(40min) = rechercher sur internet des modéles d'emoji

* Puis copier/coller dans le code.

* redux.js.org pour de la doc
