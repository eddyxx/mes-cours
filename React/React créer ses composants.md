## React et ses composants



Les composants vous permettent de découper l’interface utilisateur en éléments indépendants et réutilisables,  permettant ainsi de considérer chaque élément de manière isolée.

Conceptuellement, les composants sont comme des fonctions JavaScript. 

Ils acceptent des entrées quelconques (appelées « props ») et renvoient des éléments React décrivant ce qui doit apparaître à l’écran.

**Le moyen le plus simple de définir un composant consiste à écrire une fonction JavaScript :**

````
function Welcome(props) {

  return <h1>Bonjour, {props.name}</h1>;
}

=> Cette fonction est un composant React valide car elle accepte un seul argument « props » (qui signifie « propriétés ») contenant des données, et renvoie un élément React. Nous appelons de tels composants des « fonctions composants », car ce sont littéralement des fonctions JavaScript.
````

React considère les composants commençant par des lettres minuscules comme des balises DOM. 

  Par exemple:
  
````
Que vous déclariez un composant sous forme de fonction ou de classe, il ne doit jamais modifier ses propres props.
 
(<div/>) représente une balise HTML div, mais (<Welcome />) représente un composant, et exige que l’identifiant Welcome existe dans la portée courante.

Tout composant React doit agir comme une fonction pure vis-à-vis de ses props.

Considérons cette fonction sum :
-----------------------------------------------------------------------------------
                       function sum(a, b) {            
                                
                            return a + b;                 
  
                               }
------------------------------------------------------------------------------------

=> Ces fonctions sont dites « pures » parce qu’elles ne tentent pas de modifier leurs entrées et retournent toujours le même résultat pour les mêmes entrées.

-------------------------------------------------------------------------------------

                        function withdraw(account, amount) {

                                account.total -= amount;

                                      }
--------------------------------------------------------------------------------------
=> En revanche, cette fonction est impure car elle modifie sa propre entrée :
````
