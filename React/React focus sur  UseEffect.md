## React Focus sur UseEffect

Les Hooks sont une nouveauté de React 16.8. c'est de la programmation fonctionnel appliquer a react.

Ils permettent de bénéficier d’un état local et d’autres fonctionnalités de React sans avoir à écrire de classes.


````
Le Hook d’effet permet l’exécution d’effets de bord dans les fonctions composants .

Ex:

import React, { useState, useEffect } from 'react';

function Example() {

  const [count, setCount] = useState(0);

  // Similaire à componentDidMount et componentDidUpdate :
  
  useEffect(() => {
  
    // Met à jour le titre du document via l’API du navigateur
    
    document.title = `Vous avez cliqué ${count} fois`;
    
  });

  return (
  
    <div>
    
      <p>Vous avez cliqué {count} fois</p>
      
      <button onClick={() => setCount(count + 1)}>
      
        Cliquez ici
        
      </button>
      
    </div>
    
  );
  
}
````
**Charger des données depuis un serveur distant, s’abonner à quelque chose et modifier manuellement le DOM sont autant d’exemples d’effets de bord.**

///video103///
