## React interpolation :

[L'interpolation] permet d'afficher du texte dynamiquement dans le template d'un composant. 

Ce texte vient généralement d'une propriété ou d'une méthode de la classe associée au template. 

Ça peut également être un petit bout de code JavaScript qui sera évalué, par exemple un opérateur ternaire.

ex:

* Une div (htlm) qui va arriver sur un site internet ou la valeur (version) va être calculer.

[une interpolation] est un calcule qui est fait a l'interieur d'un template.
