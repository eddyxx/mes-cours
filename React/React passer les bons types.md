## Passer les bon types

Quand on veut passer en react des props sous la forme de number ,il faut les interpoler.

````
function hello({firstname, lastname, age})

return (

 <div>

  age: {typeof age}


 Bonjour je m'appelle {firstname }{lastname} et j'ai {age} an(s)!

 </div>

);

}

const exampleDiv = <hello

firstname= 'elliot'

lastname= 'bobino'

age= {17} [instead of => age= '17' , le chiffre est presenter sous la forme d'un string]
````

Il faut preciser quand on veut renvoyer autre chose qu'un type string (exemple ci dessus)

Quand je veut passer un type bolean , il faut utiliser l'interpolation.

````
function hello({firstname, lastname, age, car})

return (

 <div>

  age: {typeof age}

  car: {typeof car}

 Bonjour je m'appelle {firstname }{lastname} et j'ai {age} an(s)!

 </div>

);

}

const exampleDiv = <hello

firstname= 'elliot'

lastname= 'bobino'

age= {17}

car= {true} => interpolation
````
