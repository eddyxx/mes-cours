## React spécifier les typas des props

[unpkg.com/prop-types] =  librairie qui permet de spécifier les types que prends un componant

dans ses propos.

On va ecrire Hello (rappel de la fonction hello , celle du componant)

on va lui rajouter l'attribut propstype [hello.propsTypes]


````
hello.propTypes = {

fisrtName: propTypes.string.isRequired,

lastName: propTypes.string.isRequired,

age: propTypes.number.isRequired,

car: propTypes.bool,

};
````


**Memo**

* [required] = indispensable au bon fonctionnement du code

* [http://github.com/facebook/prop-types] => liste de tous ce qui peut être declarer
