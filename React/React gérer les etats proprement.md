## Gerer les états proprement

React nous propose un outil pour pouvoir gerer les états et les state proprement.

ex:

Dans le cas ou j'ai 3 états a gérer:

````
<script type= "text/babel">

const state={count:0, lastEventType: '', name: ''};

 function app() {
 
 [state, setState] =({react.useState({count:0,lastEventType:'', name: ''});
 
function handleEvent (event) {

setState({count:state.count + 1, lastEventType : event.type});

}

function handleInputChange (event) {

console.log(event.target.value);

setState({name: event.target.value});

}

return (

<>

<h1>bonjour {state.name}</h1>

<h1>Nombre d'events:{state, count }</h1>

<h1>dernier event :{state, lastEventType }</h1>

<div

    onClick={handleEvent}

   onMouseEnter={handleEvent}

   onMouseLeave={handleEvent}

   onDragStart="" {handleEvent}
>

</div>

  <input type="text" onChange={handleEvent}/>

</>

  );

}
````
*  On peut destructurer state => {...state}
````
<script type= "text/babel">

const state={count:0, lastEventType: '', name: ''};

 function app() {
 
 [state, setState] =({react.useState({count:0,lastEventType:'', name: ''});
 
function handleEvent (event) {

setState({...state,

count:state.count + 1, 

lastEventType : event.type,

});

}

function handleInputChange (event) {

console.log(event.target.value);

setState({...state,name: event.target.value});

}
````
Avec cet méthode, a chaque fois on récupere l'ancien état state, (sauvegarde) ainsi on ne perd pas les données.

Ne pas faire un RenderApplication car react va le faire automatiquement ,il suffit  d' utiliser l'outil [ NewState]

///video91///
