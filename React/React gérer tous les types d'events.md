## Gérer tous les types d'events


* [handleEvent] => permet de gerer tout type d'evenement

* [lastEventType] => va nous donner le dernier event survenue.

* [Event] => c'est l'évenement dans le navig que l'on recupere au moment ou l'on fait un onclick

-------------------------------------------------------------------------------------------------
**ex d'event:**

* onMouseEnter={handleEvent} => quand la souris entre par dessus un élément.

* onMouseout={handleEvent}   => Cet evenement ne s'adresse qu'au bloc lui même, et s'exectute dès que l'on entre dans un autre bloc qu'il soit externe ou interne. 

 Le bloc rouge est différencié du bloc vert.

* onMouseLeave={handleEvent} => Contrairement à mouseout, mouseleave comprend tous les enfants. 

Cet événement ne s'execute donc que si vous sortez du bloc et de tout ce qu'il contient.

* onDragStart={handleEvent}  => quand on clic sur un element et qu'on essaie de le deplacer.

````

<script type= "text/babel">

 function app() {

*function handleEvent (event) {

setState({count: this.state.count + 1, lastEventType : event.type});

}

return (

<>

<h1>Nombre de clicks:{state, count }</h1>

<h1>dernier event :{state, lastEventType }</h1>

<button>onClick={handleEvent()}</button>

</>

);

}
````


