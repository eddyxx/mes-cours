## React - Les fragments:

[Fragments.] En React: 

il est courant pour un composant de renvoyer plusieurs éléments. 

Les fragments nous permettent de grouper une liste d'enfants sans ajouter de nœud (div) supplémentaire au DOM. 

Il existe aussi une nouvelle syntaxe concise pour les déclarer.

ex:

[</>] = c'est un contenaire qui va contenir des div et qui va nous permettre d'ecrire juste en [JSX].
