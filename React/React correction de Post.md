## React correction de post

Quand on fait un post en back end , il faut systematiquement renvoyer l' Uid creer.

On veut que l'UseEffect se joue quand mon submit se remet a jour  submit decide si le useState de passe en true ou en false .

=> dans mon handleSubmit si je veut verifier que mes datas sont bonnes

=> si les données sont fausses, je freine setSending a [false] puis je creer un message d'erreur dans set message.

````

ex:

const [roomNumbers, setRoomNumbers] = React.useState('');

const [sending, setSending] =React.useState(false);

React.useEffect(()=> { 

async function postHotel({name, roomNumbers}) {

if(!sending) {

return;

}

if (!name|| ! roomNumbers) {

setMessage('error missing data');

setSending(false);

return;

}
setMessage('posst in progress');

try {

const response = await axios.post(api, {name, roomNumbers});

setMessage('la chambre crée a l'uid : $ {response.data.id}');

} catch (e) {

setMessage(e.data.message);

}
postHotel({name, roomNumbers});

},[sending]);

function handleSubmit(event) {

event.preventDefault();

if (!name|| !roomNumbers) {

setMessage('error missing data');

setSending(false);

return;

}

setSending(true);

)

````
#### Memo:

**Pourquoi a la fin il y a un setSending(false) ?** 
 
=> Car a chaque fois qu il change de valeur , on veut que react trigger le useEffect.

**c'est quoi un  trigger ?**

=> En programmation procédurale, un déclencheur (trigger en anglais) est un dispositif logiciel qui provoque un traitement particulier en fonction d'événements prédéfinis. Par extension, c'est l'événement lui-même qui est qualifié de déclencheur.

///video104///
