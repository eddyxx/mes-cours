## React Affichage conditionnel

### A. Qu'est ce que l'Affichage conditionnel ?

En React, On peut concevoir des composants distincts qui encapsulent le comportement voulu. 

On peut alors n’afficher que certains d’entre eux, suivant l’état de l'application.

L’affichage conditionnel en React fonctionne de la même façon que les conditions en Javascript. 

On utilise l’instruction Javascript if ou l’opérateur ternaire pour créer des éléments représentant l’état courant, et on laisse React mettre à jour l’interface utilisateur (UI) pour qu’elle corresponde.

**ex:**

Je veut afficher ,en fonction de certain cas ,sois le composent A  , le component B  ou le component C .

````
function A() {

return <h1> je suis A</h1>;

}

function B() {

return <h1>je suis B</h1>;

}

function C() {

return <h1>je suis C</h1>;

}
function ManageDisplay(props) {

console.log(props)

return <pre>{JSON.stringify(propos)}<pre>

}

function app() {

return <>

<htlm value= {1}/>

</>;

}

reactDOM.render(<app/>, document.getElementById('root'));

</script>

</body>

</htlm>
````

- [manageDisplay] = gére le display

- [JSON.stringify(props)] = sert a transformer en un string les props .

--------------------------------->**Version Destructurer + switch**<----------------------------------

````
function A() {

return <h1> je suis A</h1>;

}

function B() {

return <h1>je suis B</h1>;

}

function C() {

return <h1>je suis C</h1>;

}
function ManageDisplay({value}) {

console.log(value) {

switch (value) {

case: 1
               
         return <A/>;

case:2

          return <B/>;
}
case:3

         return <C/>;

default:
      
       return null

   }

}

function app() {

return <>

<ManageDisplay value= {1}/>

</>;

}

ReactDOM.render(<app/>, document.getElementById('root'));

</script>

</body>

</htlm>
````
------------------------------>**Methode avec if()**<-------------------------------

````
function A() {

return <h1> je suis A</h1>;

}

function B() {

return <h1>je suis B</h1>;

}

function C() {

return <h1>je suis C</h1>;

}
function ManageDisplay(props) {

console.log(props)

if (value === 1) {

return <A/>

}


if (value === 2) {

return <B/>

}


if (value === 3) {

return <C/>

}


return null

}

function app() {

return <>

<ManageDisplay value= {1}/>

</>;

}

reactDOM.render(<app/>, document.getElementById('root'));

</script>

</body>

</ManageDisplay>
````
[switch] =>  commence par évaluer l'expression fournie (cette évaluation ne se produit qu'une fois). 

Si une correspondance est trouvée, le programme exécutera les instructions associées. 

Si plusieurs cas de figure correspondent, le premier sera sélectionné (même si les cas sont différents les uns des autres).

Le programme recherche tout d'abord une clause case dont l'expression est évaluée avec la même valeur que l'expression d'entrée (au sens de l'égalité stricte. 

Si une telle clause est trouvée, les instructions associées sont exécutées. 

Si aucune clause case n'est trouvée, le programme recherche la clause optionnelle default et si elle existe, les instructions correspondantes sont exécutées. 

Si cette clause optionnelle n'est pas utilisée, le programme continue son exécution après l'instruction switch. Par convention, la clause default est utilisée en dernière mais cela n'est pas nécessaire.    
         
