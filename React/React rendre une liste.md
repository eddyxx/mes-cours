## React rendre une liste

### A. Comment rendre une liste ?



````
<script type="text/babel">

function Touche(props) {

return <button>{props.display}</button>;

}

const liste = ['a', 'b','c'];

function Calc() {

return <>

<Touch display={0}></Touch>


</>;

}

function app() {

return <>

<Calc/>

</>;

}
````

**[Message warning console]** : Each child in a list should have a unique "key" props...

=> Il faut qu'on puisse identifier a l'aide d'une clé les infos contenues dans la liste  pour des raison de performance.

----------------------------------------->**Solution**<----------------------------------------------------------------------
**On se base sur un array:**

````
<script type="text/babel">

function Touche(props) {

return <button>{props.display}</button>;

}

const liste = ['a', 'b','c'];

function Calc() {

return liste.map ((value, index)) => 

<Touche display={value} key={index}/>),

);

}

function app() {

return <>

<Calc/>

</>;

}
````
///video97///
