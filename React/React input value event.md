## React input value event

### Comment gérer les events sur un input?

si on veut avoir la valeur il faut tomber sur la valeur primitive, dans le cas ci dessous, c'est { event.target.value}

````
<script type= "text/babel">

const state={count:0, lastEventType: ''};

 function app() {
 
function handleEvent (event) {

setState({count:state.count + 1, lastEventType : event.type});

}

function handleInputChange (event) {

console.log(event.target.value);

setState({name: event.target.value});

}

return (

<>

<h1>bonjour {state.name}</h1>

<h1>Nombre d'events:{state, count }</h1>

<h1>dernier event :{state, lastEventType }</h1>

<div

    onClick={handleEvent}

   onMouseEnter={handleEvent}

   onMouseLeave={handleEvent}

   onDragStart="" {handleEvent}
>

</div>

  <input type="text" onChange={handleEvent}/>

</>

  );

}
````

- [Input] :

Les éléments <input> dont l'attribut type vaut number permettent à un utilisateur de saisir des nombres dans un formulaires.
 
- [value]:
 
Un nombre (cf. Number) qui représente la valeur saisie dans le contrôle. Il est possible d'indiquer une valeur par défaut en utilisant l'attribut value .


///video90///
